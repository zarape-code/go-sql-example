package dao

import (
	"database/sql"

	"github.com/ethien-salinas/sql-go-example/data/model"
)

// Users struct which can access to the database
type Users struct {
	DB *sql.DB
}

// GetDetail of a user based on their Id
func (u *Users) GetDetail(id int) (*model.User, error) {
	var user model.User
	if err := u.DB.QueryRow("Select * FROM user WHERE id = $1", id).Scan(
		&user.ID,
		&user.FirstName,
		&user.LastName,
		&user.Email,
		&user.Gender,
		&user.IPAddress,
		&user.Avatar,
	); err != nil {
		return nil, err
	}
	return &user, nil
}
