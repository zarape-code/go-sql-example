package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/ethien-salinas/sql-go-example/data"
	"github.com/ethien-salinas/sql-go-example/data/dao"
	"github.com/joho/godotenv"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	// Load env variables
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	dn := os.Getenv("SQL_DRIVER_NAME")
	ds := os.Getenv("SQL_CONN_STRING")
	// Open DB conn
	db, err := data.Open(dn, ds)
	if err != nil {
		log.Fatal("unable to connect to the database: ", err)
	}
	defer db.Close()
	// Generate a random number
	rand.Seed(time.Now().UnixNano())
	min := 1
	max := 1000
	// Conn passing to the DAO and use through a method
	users := dao.Users{DB: db}
	user, err := users.GetDetail(rand.Intn(max-min+1) + min)
	if err != nil {
		log.Fatal("unable to retreive user: ", err)
	}
	fmt.Println(user)
}
