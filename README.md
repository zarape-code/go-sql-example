# Go SQL example

Ejemplo de una conexión a BD usando estructuras y SQL plano

## Iniciar el proyecto
Generar el archivo `.env` con las keys necesarias
```sh
$ mv .env.development .env
```
Arrancar el proyecto para probar la carga de un usuario
```sh
$ go run main.go
```
Compilar proyecto
```sh
$ go build -o go-sql-example.e
```
Ejecutar binario 
```sh
$ ./go-sql-example.e
```
Construir y ejecutar binario para probar
```sh
$ go build && ./go-sql-example.e
```

## Estructura del proyecto
```sh
\                   # archivos de configuración
|-main.go           # archivo pricipal del proyecto
|-data/             # carpeta con código de acceso a BD
    |-dao/          # conjunto de objetos conectados a la BD
    |---users.go    # estructura conectada a la BD
    |-model/        # conjunto de modelado de atributos
    |---user.go     # modelado de los atributos de user (coinciden con los que tiene la tabla en BD)
```

## Tecnología empleada
* [Go](https://golang.org/) - Lenguaje de programación
* [GoDotEnv](https://github.com/joho/godotenv) - carga de variables de entorno en archivo `.env`
* [go-sqlite3](https://github.com/mattn/go-sqlite3) - sqlite3 driver

___
### Autor
Creado por [Ethien Salinas](https://www.linkedin.com/in/ethiensalinas/) para zarape.io 🇲🇽
